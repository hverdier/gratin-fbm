# from gratin_exp.training.callbacks import Plotter
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint, EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from gfbm.data_tools.datamodule import DataModule
from gfbm.training.callbacks import fBMPlotter
from gfbm.layers.BFlow import BFlowEncoder
import pytorch_lightning as pl
# from codecarbon import track_emissions

import sys
import os
import secrets

export_model_path = os.path.join(os.environ["WORK"], "models")


def get_model_folder(version):
    return os.path.join(export_model_path, version)


# @track_emissions(project_name="BF_fBM")
def train_encoder(hparams, cluster):

    BATCH_SIZE = min(hparams.batch_size, 128)

    if hparams.L >= 800:
        BATCH_SIZE = min(64, BATCH_SIZE)
        print("Reducing batch size down to %d" % BATCH_SIZE)
    if hparams.L >= 3000:
        BATCH_SIZE = min(32, BATCH_SIZE)
        print("Reducing batch size down to %d" % BATCH_SIZE)
    if hparams.L >= 10000:
        BATCH_SIZE = min(4, BATCH_SIZE)
        print("Reducing batch size down to %d" % BATCH_SIZE)

    dl_params = {
        "batch_size": BATCH_SIZE,
        "num_workers": 12,
    }
    try:
        dl_params["num_workers"] = hparams.num_workers
    except:
        pass

    # assert hparams.batch_size % dl_params["batch_size"] == 0

    vary_T = True if "diff" in hparams.mode and (not hparams.explore_noise) else False
    vary_tau = True if "tau" in hparams.mode else False

    targets = {"alpha": "alpha", "tau": "log_tau", "diff": "log_diffusion"}

    if not vary_T:
        length_range = (hparams.L, hparams.L + 1)
    else:
        length_range = (10, hparams.L + 1)

    encoder_params = {
        "n_c": 32,
        "latent_dim": 16,
        "alpha_range": (0.4, 1.6),
        "length_range": length_range,
        # Arguments about the graph are passed to the encoder
        # to allow easy initilization of the datamodule when using it to predict true data
        # They're not used ny the encoder during training, but only by the datamodule
        "edge_method": "uniform_min_%d" % hparams.L if hparams.sparse else "geom_causal",
        "degree": 20 if hparams.sparse else 20,
        "dim": 1,
        "scale_types": ["step_sum"] if hparams.sparse else ["step_std", "step_sum", "pos_std"],
        "vary_T": vary_T,
        "vary_tau": vary_tau,
        "noise_sigma": hparams.noise_sigma,
        "lr": hparams.learning_rate,
        "n_lengths": 16,  # n lengths for eval batches
        "T": hparams.L,
        "skip_scales": hparams.skip_scales,
        "to_predict": targets[hparams.to_predict],
    }

    trainer_params = {
        "gpus": 1,
        "terminate_on_nan": True,
        "reload_dataloaders_every_epoch": True,
        "track_grad_norm": 2,
        "accumulate_grad_batches": max(
            1, int(hparams.batch_size // dl_params["batch_size"])
        ),
        "deterministic": False,
        "gradient_clip_val": 10.0,
    }

    dm = DataModule(
        dl_params=dl_params,
        ds_params={"N": int(1e5),
                   "dim": encoder_params["dim"],
                   "length_range": length_range,
                   "vary_tau": vary_tau,
                   "noise": hparams.noise_sigma},
        graph_info={"edge_method": encoder_params["edge_method"],
                    "degree": encoder_params["degree"]},
        no_parallel=True,
    )
    dm.setup()

    model = BFlowEncoder(**encoder_params).cuda()

    print(model.hparams)

    mode_indicator = "AD" if hparams.mode == "alpha_diff" else "AT"
    TAG = "BF_%s_encoder_L%d_bs%d_LR_%.1e_%s_noise_%.2f_%s" % (
        hparams.to_predict,
        hparams.L,
        hparams.batch_size,
        hparams.learning_rate,
        mode_indicator,
        hparams.noise_sigma,
        secrets.token_hex(2)
    )
    TAG_VERSION = "%s_%s" % (TAG, hparams.version)

    TB_PATH = os.path.join(os.environ["SCRATCH"], "tb_logs", hparams.version)
    if not os.path.exists(TB_PATH):
        os.mkdir(TB_PATH)

    TB = TensorBoardLogger(
        default_hp_metric=False,
        save_dir=TB_PATH,
        name=TAG_VERSION + "_BS%d" % hparams.batch_size,
    )
    PLT = fBMPlotter()
    LR = LearningRateMonitor("epoch")
    model_ID = "%s_%d" % (TAG_VERSION, encoder_params["T"])

    model_folder = get_model_folder(hparams.version)
    if not os.path.exists(model_folder):
        try:
            os.mkdir(model_folder)
        except FileExistsError:
            pass  # Sometimes, two concurrent processes create the folder at the same time
    assert os.path.exists(model_folder)

    CKPT = ModelCheckpoint(
        dirpath=model_folder,
        filename=model_ID,
        verbose=True,
        monitor="training_loss_epoch",
        mode="min",
    )

    ES = EarlyStopping("training_loss_epoch", patience=3)

    trainer = pl.Trainer(
        logger=TB,
        callbacks=[LR, CKPT, ES, PLT],
        weights_save_path=model_folder,
        **trainer_params,
    )

    trainer.fit(model=model, datamodule=dm)
    trainer.test(model=model, datamodule=dm)
    TB.finalize(status="success")
