from test_tube.hpc import SlurmCluster


def get_cluster_object(host_name, hyperparams, job_time="08:00:00"):
    if "maestro" in host_name:
        cluster = SlurmCluster(
            hyperparam_optimizer=hyperparams,
            log_path="/pasteur/sonic/homes/hverdier/slurm_output",
            python_cmd="singularity exec \
                --nv \
                -B /pasteur/sonic/scratch/users/hverdier:/scratch \
                -B /pasteur/zeus/projets/p02/hecatonchire/hippo/fBM:/hecat \
                -H $HOME:/home /pasteur/sonic/scratch/users/hverdier/gratin.sif python3",
        )
        cluster.add_slurm_cmd(
            cmd="partition", value="gpu", comment="require the gpu partition"
        )
        cluster.add_slurm_cmd("qos", "gpu", "qos gpu")

        # cluster.add_command("source /opt/gensoft/adm/etc/profile.d/modules.sh")

        cluster.add_command(
            "source /opt/gensoft/adm/etc/profile.d/modules.sh && echo sourced"
        )
        cluster.add_command("module purge")
        cluster.add_command("module load singularity && echo loaded")
        cluster.add_command("module li")
        cluster.add_command("hostname")
        cluster.add_command("nvidia-smi")
        cluster.add_command("pwd")
        cluster.add_command("ls")
        cluster.gpu_type = "voltaV100"

    else:  # Assume we're on Jean-Zay
        cluster = SlurmCluster(
            hyperparam_optimizer=hyperparams,
            log_path="/linkhome/rech/genpro01/uxo83vo/slurm_output",
            enable_log_err=True,
            # python_cmd="singularity exec \
            #    --nv \
            #    -B $SCRATCH:/scratch \
            #    -B $WORK:/hecat \
            #    -H $WORK:/home $SINGULARITY_ALLOWED_DIR/gratin.sif python3",
        )
        cluster.add_slurm_cmd("account", "vjk@gpu", "account with GPU usage")
        # cluster.add_slurm_cmd("qos", "qos_gpu-t4", "QOS for long runs")
        cluster.add_slurm_cmd("partition", "gpu_p2", "partition with GPUs")
        # cluster.add_slurm_cmd(
        #    "constraint", "v100-32g", "Request GPUs with a lot of memory"
        # )
        cluster.add_slurm_cmd("hint", "nomultithread", "advised by IDRIS doc")

        cluster.add_command("pwd")
        cluster.add_command("module purge")
        cluster.add_command("module load pytorch-gpu/py3/1.9.0")
        #cluster.add_command("module load singularity && echo loaded")

    # let the cluster know where to email for a change in job status (ie: complete, fail, etc...)
    # cluster.notify_job_status(email='some@email.com', on_done=True, on_fail=True)

    # set the job options. In this instance, we'll run 20 different models
    # each with its own set of hyperparameters giving each one 1 GPU (ie: taking up 20 GPUs)
    cluster.per_experiment_nb_gpus = 1
    cluster.per_experiment_nb_nodes = 1
    cluster.per_experiment_nb_cpus = 12
    cluster.call_load_checkpoint = False

    # we'll request 10GB of memory per node
    cluster.memory_mb_per_node = 10000

    # set a walltime of 10 minues
    # cluster.job_time = "00:10:00"
    # cluster.job_time = "80:00:00"
    cluster.job_time = job_time

    # 1 minute before walltime is up, SlurmCluster will launch a continuation job and kill this job.
    # you must provide your own loading and saving function which the cluster object will call
    # cluster.minutes_to_checkpoint_before_walltime = 2

    return cluster
