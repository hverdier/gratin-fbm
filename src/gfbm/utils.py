import numpy as np
from scipy.linalg import pinvh
import warnings


def fbm_covar(L, alpha, sigma):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        C = np.fromfunction(
            lambda i, j: np.exp(alpha * np.log(np.abs(i - j))), (L, L), dtype=np.float64
        )
        np.fill_diagonal(C, 0.0)
        B = np.fromfunction(
            lambda i, j: np.exp(alpha * np.log(np.abs(i))), (L, L), dtype=np.float64
        )
        A = np.fromfunction(
            lambda i, j: np.exp(alpha * np.log(np.abs(j))), (L, L), dtype=np.float64
        )
        S = np.copy(0.5 * (A + B - C))
        S = S[1:]
        S = S[:, 1:]
        np.fill_diagonal(S, np.diagonal(S) + sigma**2)

        C = np.fromfunction(
            lambda i, j: np.log(np.abs(i - j)) * np.exp(alpha * np.log(np.abs(i - j))),
            (L, L),
            dtype=np.float64,
        )
        np.fill_diagonal(C, 0.0)
        B = np.fromfunction(
            lambda i, j: np.log(np.abs(i)) * np.exp(alpha * np.log(np.abs(i))),
            (L, L),
            dtype=np.float64,
        )
        A = np.fromfunction(
            lambda i, j: np.log(np.abs(j)) * np.exp(alpha * np.log(np.abs(j))),
            (L, L),
            dtype=np.float64,
        )
        d_S = np.copy(0.5 * (A + B - C))
        d_S = d_S[1:]
        d_S = d_S[:, 1:]

    S_inv = pinvh(S)

    return S, d_S, S_inv


def min_variance(d_C, C_inv):
    I = 0.5 * np.trace(C_inv @ d_C @ C_inv @ d_C)
    return 1.0 / I


def ML_matrices_just_alpha(Ns, alphas):
    Cs = {}
    inv_Cs = {}
    logdet = {}
    for alpha in alphas:
        for N in Ns:
            Cs[(alpha, N)], _, inv_Cs[(alpha, N)] = fbm_covar(N, alpha, 0.0)
            logdet[(alpha, N)] = np.linalg.slogdet(Cs[(alpha, N)])[1]
    return Cs, inv_Cs, logdet


def estimate_true_posterior(traj_p, logdets, inv_Cs, alpha_values):

    p = np.reshape(traj_p[1:], (-1, 1))
    N = traj_p.shape[0]
    L = np.zeros((alpha_values.shape[0]))
    d_alpha = alpha_values[1] - alpha_values[0]

    for i, alpha in enumerate(alpha_values):
        # linalg.slogdet(a)[1] = log of det of a. ([0] is the sign, always positive here)
        L[i] = -0.5 * logdets[(alpha, N)] - 0.5 * (p.T @ inv_Cs[(alpha, N)] @ p)[0, 0]
    L -= np.max(L)
    p = np.exp(L)
    p /= np.sum(p) * d_alpha
    return alpha_values, p
