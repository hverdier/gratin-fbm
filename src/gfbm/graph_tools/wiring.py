import numpy as np
import math


def complete_graph(N):
    edge_start = np.array([i // N for i in range(N ** 2) if (i % N) != i // N])
    edge_end = np.array([i % N for i in range(N ** 2) if (i % N) != i // N])
    return edge_start, edge_end


def edges_uniform(N, D):
    # N = trajectory length
    # D = out degree per node
    #delta = 1 + np.random.choice(N - 1, size=N * D, replace=True)
    #edge_start = np.tile(np.arange(N), D)
    #edge_end = (edge_start + delta) % N
    #assert np.min(np.abs(edge_end - edge_start)) > 0
    # return edge_start, edge_end

    return edges_uniform_min(N, D, max_length_training=np.inf, to_wire=np.arange(N) >= 0)


def edges_uniform_min(N, D, max_length_training, to_wire):
    
    # max_length_training was seen during training
    # meaning that edges of length 1 accounted for 1/max_length_training of time
    # Here, we restrict edge length so that it is not shorter than 1/max_length_training in fraction of time
    # to_wire est un tableau de booleens qui disent si on doit considérer les noeuds pour les arrêtes

    D = min(N - 1, D)  # the degree cannot be larger than the length
    #min_ = max(1, math.ceil(N / max_length_training))
    # En fait min_ ne sert à rien parce qu'on sample de toutes façons les noeuds de façon régulière
    min_ = 1

    # delta = np.concatenate([np.random.choice(-)])
    #delta = np.random.choice([k for k in range(-max_, max_ + 1) if k != 0], size=N * D, replace=True)
    #edge_start = np.tile(np.arange(N), D)
    edge_starts = []
    edge_ends = []
    edge_options = np.arange(N)[to_wire]
    n_edges = D
    for i in edge_options:
        edge_starts.append(np.array([i for _ in range(n_edges)]))
        edge_ends.append(np.array(np.random.choice(
            edge_options[np.abs(edge_options - i) >= min_], size=(n_edges,), replace=False)))
        # edge_ends.append(np.array(np.random.choice(np.where(
        #    np.abs(edge_options - i) >= min_)[0],
        #    size=(n_edges,), replace=False))
        # )

    edge_start = np.concatenate(edge_starts)
    edge_end = np.concatenate(edge_ends)
    
    assert np.sum(np.abs(edge_end - edge_start) < min_) == 0
    return edge_start, edge_end


def edges_geom_causal(N, D):
    # 0,0,0,...,N-1,N-1,N-1
    step_sizes = np.unique(np.floor(np.logspace(0, np.log10(N - 1), D)))
    # reduce mean degree to prevent doubled edges
    D = len(step_sizes)
    edge_start = np.repeat(np.arange(N), D)
    # A,B,C,...,A,B,C
    delta = -np.tile(step_sizes, N)
    edge_end = (edge_start + delta) % N

    return edge_start, edge_end
