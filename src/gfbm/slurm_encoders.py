from test_tube import HyperOptArgumentParser

import socket

from train_encoder_BF import train_encoder
from gfbm.cluster.init_clusters import get_cluster_object


if __name__ == "__main__":

    parser = HyperOptArgumentParser(strategy="grid_search", add_help=False)
    parser.add_argument("--version", type=str)
    parser.opt_list(
        "--learning_rate",
        default=1e-3,
        type=float,
        options=[1e-3],
        tunable=True,
    )

    parser.opt_list("--L", default=100, type=int, options=[100], tunable=False)
    parser.opt_list("--noise_sigma", default=0., type=float,
                    options=[0., 0.25, 0.5, 0.75, 1., 2., 3., 5.], tunable=False)
    parser.opt_list(
        "--batch_size",
        default=128,
        type=int,
        options=[128],
        tunable=True,
    )

    parser.add_argument('--explore_noise',
                        help="whether we are or not in a scenario where various levels of noise are explored",
                        dest='explore_noise', action='store_true')
    parser.set_defaults(explore_noise=False)

    parser.add_argument("--skip_scales",
                        help="if true, scales are never seen by the network",
                        dest="skip_scales", action="store_true")
    parser.set_defaults(skip_scales=False)

    parser.add_argument('--sparse',
                        dest='sparse', action='store_true')
    parser.set_defaults(sparse=False)

    parser.opt_list(
        "--mode",
        default="alpha_tau",
        options=["alpha_tau", "alpha_diff"],
        tunable=False,
    )

    parser.opt_list(
        "--to_predict", default="alpha", options=["alpha", "tau", "diff"], tunable=False
    )

    hyperparams = parser.parse_args()

    # init cluster
    host_name = socket.gethostname()
    print("host_name is %s" % host_name)

    cluster = get_cluster_object(host_name, hyperparams)

    # run the models on the cluster

    cluster.optimize_parallel_cluster_gpu(
        train_encoder,
        nb_trials=1 if hyperparams.explore_noise else 12,
        job_name="BF_%s" % hyperparams.version,
        job_display_name="BF",
        enable_auto_resubmit=False,
    )
