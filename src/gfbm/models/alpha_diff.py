from typing import Any, List
import torch
import pytorch_lightning as pl
from gfbm.layers.encoders import TrajsEncoder
from gfbm.layers.features_init import TrajsFeatures, TrajsFeaturesSimple
from gfbm.layers.InvNet import InvertibleNet
from gfbm.data_tools.skeleton_data import SkeletonTrajData
from torch.optim.lr_scheduler import ExponentialLR


class AlphaDiff(pl.LightningModule):
    def __init__(
        self, encoder, n_c: int = 12, latent_dim: int = 12, scales: List = ["pos_std"]
    ):
        super().__init__()
        self.save_hyperparameters(ignore="encoder")
        print(self.hparams["scales"])
        self.dim_theta = 2
        self.features_maker = TrajsFeaturesSimple()
        self.encoder = encoder
        self.invertible_net = InvertibleNet(
            dim_theta=2,
            dim_x=latent_dim,
            n_blocks=3,
            s_abs=3,
            t_abs=3,
        )

    def compute_features(self, x: SkeletonTrajData):
        X, E, scales = self.features_maker(x)
        x.scales = torch.cat(
            [scales[k].view(-1, 1) for k in self.hparams["scales"]], dim=1
        )
        x.x = X
        x.adj_t = x.adj_t.set_value(E, layout="coo")
        return x

    def forward(self, x: SkeletonTrajData, sample: bool = False, n_repeats: int = 10):
        x = self.compute_features(x)

        h = self.encoder(x)

        if not sample:
            z, log_J = self.invertible_net(x.theta, h, inverse=False)
            return z, log_J

        elif sample:
            z = torch.normal(
                mean=0.0,
                std=1.0,
                size=(n_repeats * h.shape[0], self.dim_theta),
                device=h.device,
            )
            h = h.repeat_interleave(n_repeats, 0)

            theta = self.invertible_net(z, h, inverse=True)
            true_theta = x.theta

            theta = torch.transpose(
                theta.view(h.shape[0] // n_repeats, n_repeats, 2), 1, 2
            )
            return (theta, true_theta, x.length)

    def predict_step(self, batch: Any, batch_idx: int, dataloader_idx: int = 0) -> Any:
        return self.forward(batch, sample=True, n_repeats=50)

    def training_step(self, batch, batch_idx, optimizer_idx=0):
        z, log_J = self.forward(batch)
        z_norm2 = torch.sum(z**2, dim=1)
        l = torch.mean(0.5 * z_norm2 - log_J, dim=0)

        self.log(
            "training_loss",
            value=l,
            on_step=True,
            on_epoch=True,
            prog_bar=True,
            batch_size=log_J.shape[0],
        )
        self.log(
            "z_norm",
            value=torch.mean(z_norm2),
            on_step=True,
            prog_bar=True,
            batch_size=log_J.shape[0],
        )
        self.log(
            "log_J",
            value=torch.mean(log_J),
            on_step=True,
            prog_bar=True,
            batch_size=log_J.shape[0],
        )

        return l

    def validation_step(self, batch, batch_idx, optimizer_idx=0):
        B = batch.alpha.shape[0]
        theta, true_theta, length = self.forward(batch, sample=True, n_repeats=10)
        assert theta.shape == (B, self.dim_theta, 10)
        assert true_theta.shape == (B, self.dim_theta)
        # theta : (B,dim_theta,n_repeats)
        mean_inferred_theta = torch.mean(theta, dim=-1)
        err = theta - true_theta[:, :, None]
        err_mean = mean_inferred_theta - true_theta
        mse_mean = torch.mean(err_mean**2, dim=0)
        mse_mean_upper_quartile = torch.quantile(err_mean**2, q=0.75, dim=0)
        mse_all = torch.mean(err**2, dim=0)
        mse_all_upper_quartile = torch.quantile(err**2, q=0.75, dim=0)
        var = torch.var(theta, dim=(0, 2))

        for col, name in zip([0, 1], ["log_diff", "alpha"]):
            self.logger.experiment.add_histogram(
                "spread_%s" % name,
                theta[:, col] - true_theta[:, col, None],
                global_step=self.global_step,
            )
            self.log(
                "val_MSE_mean_%s" % name,
                value=mse_mean[col],
                batch_size=B,
                on_epoch=True,
                prog_bar=True,
            )
            self.log(
                "val_MSE_mean_upper_%s" % name,
                value=mse_mean_upper_quartile[col],
                batch_size=B,
                on_epoch=True,
                prog_bar=True,
            )
            self.log(
                "val_MSE_%s" % name,
                value=mse_all[col],
                batch_size=B,
                on_epoch=True,
                prog_bar=True,
            )
            self.log(
                "val_MSE_upper_%s" % name,
                value=mse_all_upper_quartile[col],
                batch_size=B,
                on_epoch=True,
                prog_bar=True,
            )
            self.log(
                "val_Var_%s" % name,
                value=var[col],
                batch_size=B,
                on_epoch=True,
                prog_bar=True,
            )
        return theta, true_theta, length

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), amsgrad=True, lr=1e-2)
        scheduler = ExponentialLR(optimizer, gamma=0.98)
        return [optimizer], [scheduler]


class AlphaDiffGratin(AlphaDiff):
    def __init__(self, n_c: int = 32, latent_dim: int = 12):

        encoder = TrajsEncoder(
            traj_dim=1,
            x_dim=TrajsFeaturesSimple.x_dim(),
            e_dim=TrajsFeaturesSimple.e_dim(),
            n_c=n_c,
            latent_dim=latent_dim,
            n_scales=1,
        )
        scales = ["step_std"]
        super(AlphaDiffGratin, self).__init__(
            encoder=encoder, n_c=n_c, latent_dim=latent_dim, scales=scales
        )
