import numpy as np
import scipy

### FBM FUNCTIONS W/ NUMPY


def f_(DT, alpha):
    return 0.5 * (
        np.power(np.abs(DT + 1), alpha)
        + np.power(np.abs(DT - 1), alpha)
        - 2 * np.power(np.abs(DT), alpha)
    )


def f(DT, alpha, tau, lam=1):
    R = f_(DT, alpha)
    R = np.where(np.abs(DT) >= tau, R * np.exp(-lam * (np.abs(DT) - tau)), R)
    return R


def get_dx_cov(alpha, t, tau=np.inf, lam=1):
    i = np.reshape(t, (-1, 1))
    j = i.T
    k = j - i
    C = f(k, alpha, tau, lam=lam)
    # min_EV = np.min(np.linalg.eigvals(C))
    # assert min_EV > 0.0, "min EV %.2f, alpha = %.2f, tau = %d" % (min_EV, alpha, tau)

    return C


def get_fbm_from_cov(dx_cov):
    # dx = np.random.multivariate_normal(mean=np.zeros(dx_cov.shape[0]), cov=dx_cov)
    du = np.random.randn((dx_cov.shape[0]))
    L, info = scipy.linalg.lapack.dpotrf(dx_cov, lower=1)
    assert info == 0
    # checked
    # print(L @ L.T)
    # print(dx_cov)
    # gamma_inv, info = scipy.linalg.lapack.dpotri(L)
    dx = L @ du
    # print("L")
    # print(L)
    # print("dx_cov")
    # print(dx_cov)
    x = np.concatenate([np.zeros(1), np.cumsum(dx, axis=0)])
    return x


def get_fbm(D, alpha, T, tau):
    # opt_lam = 0.025  # for alpha <= 1.9, tau_c >= 10
    # opt_lam = 0.01
    opt_lam = 1.0
    try:
        C = get_dx_cov(alpha, np.arange(T - 1), tau=tau, lam=opt_lam)
        return np.stack(
            [get_fbm_from_cov(C) for i in range(D)],
            axis=1,
        )
    except Exception as e:
        print(e)
        raise