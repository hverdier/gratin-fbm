import numpy as np
from torch_geometric.data import Data
import torch
import numpy as np
from gfbm.graph_tools.wiring import *
from gfbm.data_tools import default_graph_info, default_traj_info

EMPTY_FIELD_VALUE = -999


class SkeletonTrajData(Data):
    def __init__(
        self,
        raw_positions,
        graph_info: dict = default_graph_info,
        traj_info: dict = default_traj_info,
        original_positions=None,
        **kwargs,
    ):
        if raw_positions is None:
            # print("Raw positions is none")
            # raise
            raw_positions = np.random.normal(size=(10, 1))
        else:
            pass
            # print("Raw positions has length %d" % raw_positions.shape[0])
        if original_positions is None:
            original_positions = raw_positions
        if not type(original_positions) is np.array:
            original_positions = np.array(original_positions).copy()
        dim = raw_positions.shape[1]
        default_traj_info = {
            "model": "unknown",
            "model_index": EMPTY_FIELD_VALUE,
            "drift_norm": EMPTY_FIELD_VALUE,
            "drift_vec": np.ones(dim) * EMPTY_FIELD_VALUE,
            "force_norm": EMPTY_FIELD_VALUE,
            "force_vec": np.ones(dim) * EMPTY_FIELD_VALUE,
            "log_theta": EMPTY_FIELD_VALUE,
            "alpha": EMPTY_FIELD_VALUE,
            "noise": 0.0,
            "tau": np.inf,
            "seed": EMPTY_FIELD_VALUE,
            "log_diffusion": EMPTY_FIELD_VALUE,
        }

        for key in default_traj_info:
            if default_traj_info[key] is None:
                continue
            if key not in traj_info:
                traj_info[key] = default_traj_info[key]

        raw_positions -= raw_positions[0]
        positions, clipped_steps = self.safe_positions(raw_positions, graph_info)

        X = self.get_node_features(positions, graph_info)

        # to_keep_indices = np.random.choice(X.shape[0], size=min(1000, X.shape[0]), replace=False)
        # to_keep_indices = np.linspace(
        #    0, X.shape[0], num=min(500, X.shape[0]), endpoint=False, dtype=np.int
        # )
        # Keep all !
        to_keep_indices = np.arange(X.shape[0])
        assert len(to_keep_indices) > 5
        to_keep = np.isin(np.arange(X.shape[0]), to_keep_indices)
        assert to_keep.shape[0] == X.shape[0]

        edge_index = self.get_edges(X, graph_info, to_wire=to_keep)
        E = self.get_edge_features(X, positions, edge_index, graph_info)

        def reshape(t):
            return torch.reshape(torch.from_numpy(t), (1, -1))

        def float_to_torch(t):
            return torch.Tensor([t]).view((1, 1))

        assert E.shape[0] <= X.shape[0] * graph_info["degree"], (
            E.shape,
            X.shape,
            graph_info["degree"],
        )

        super(SkeletonTrajData, self).__init__(
            pos=torch.from_numpy(positions).float(),
            x=torch.from_numpy(X).float(),
            original_pos=torch.from_numpy(original_positions).float(),
            clipped_steps=float_to_torch(clipped_steps),
            edge_index=edge_index,
            edge_attr=torch.from_numpy(E).float() if E.shape[1] > 0 else None,
            length=float_to_torch(positions.shape[0]),
            alpha=float_to_torch(traj_info["alpha"]),
            log_theta=float_to_torch(traj_info["log_theta"]),
            to_keep=torch.from_numpy(to_keep).long(),  # To keep is new
            log_tau=reshape(np.asarray(np.log10(traj_info["tau"]))),
            model=float_to_torch(traj_info["model_index"]).long(),
            noise=float_to_torch(traj_info["noise"]),
            seed=torch.Tensor([int(traj_info["seed"])]),
            log_diffusion=float_to_torch(traj_info["log_diffusion"]),
        )

    def safe_positions(self, positions, graph_info):
        # clips too long jumps, returns the number of clipped steps
        # removed
        return positions, 0

    @classmethod
    def get_edges(cls, X, graph_info, to_wire):

        D = graph_info["degree"]
        N = X.shape[0]
        if D >= N:
            edge_start, edge_end = complete_graph(N)
        elif graph_info["edge_method"] == "uniform":
            edge_start, edge_end = edges_uniform(N, D)
        elif graph_info["edge_method"] == "geom_causal":
            edge_start, edge_end = edges_geom_causal(N, D)
        elif "uniform_min" in graph_info["edge_method"]:
            edge_start, edge_end = edges_uniform_min(
                N,
                D,
                max_length_training=int(graph_info["edge_method"].split("_")[-1]),
                to_wire=to_wire,
            )
        else:
            raise NotImplementedError(f"Method { graph_info['edge_method'] } not known")
        e = np.stack(
            [np.reshape(edge_start, (-1,)), np.reshape(edge_end, (-1,))], axis=0
        )
        return torch.from_numpy(e).long()

    def get_node_features(self, positions, graph_info):
        # Minimal feature : time
        return np.reshape(np.arange(positions.shape[0]), (-1, 1)) / positions.shape[0]

    def get_edge_features(self, X, positions, edge_index, graph_info):
        if graph_info["features_on_edges"] == False:
            return np.zeros((edge_index.shape[1], 0))
        else:
            raise NotImplementedError("Ne sait pas calculer les features sur les edges")
        return None
