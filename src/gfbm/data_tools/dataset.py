from re import sub
from secrets import choice
from numpy import random
import torch_geometric.transforms as Transforms
from torch_geometric.data import Dataset
import torch
import numpy as np
from fbm import fbm
import numpy as np
from gfbm.data_tools.data import TrajData
from gfbm.data_tools.fbm_numpy import get_fbm
from torch_geometric.utils import subgraph
from scipy.stats import triang


class fBMTrajDataSet(Dataset):
    def __init__(
        self,
        N: int,
        graph_info: dict,
        length_range: tuple,  # e.g = (5,50)
        noise: float,
        vary_tau: bool,
        seed_offset: int,  # e.g. = 0):
        alpha_range: tuple = (
            0.4,
            1.6,
        ),  # if of len > 2, then taken as a set of possible values of alpha
        length_sampling: str = "uniform",  # "linear", "log"
    ):
        self.N = N

        self.seed_offset = seed_offset
        print("seed_offset = %d" % seed_offset)
        self.graph_info = graph_info
        self.length_range = length_range
        self.alpha_range = alpha_range
        self.noise = (
            noise  # localisation noise sigma is noise*sqrt(diffusion*time_delta)
        )
        self.time_delta = 1.0
        self.vary_tau = vary_tau
        self.length_sampling = length_sampling
        super(fBMTrajDataSet, self).__init__(transform=Transforms.ToSparseTensor())

    def len(self):
        return self.N

    def get_traj_params(self):
        params = {
            "alpha": np.random.uniform(self.alpha_range[0], self.alpha_range[1])
            if len(self.alpha_range) <= 2
            else np.random.choice(a=list(self.alpha_range)),
            "tau": 10.0 ** np.random.uniform(np.log10(5), np.log10(50))
            if self.vary_tau
            else np.inf,
        }
        if len(self.length_range) == 2:
            if self.length_sampling == "uniform":
                length = np.random.randint(
                    low=self.length_range[0], high=self.length_range[1]
                )
            elif self.length_sampling == "log":
                log_length = np.random.uniform(
                    low=np.log10(self.length_range[0]),
                    high=np.log10(self.length_range[1]),
                )
                length = int(np.power(10.0, log_length))
            elif self.length_sampling == "linear":
                length = int(
                    triang.rvs(
                        loc=self.length_range[0],
                        scale=self.length_range[1] - self.length_range[0],
                        c=1.0,
                    )
                )
            assert length <= self.length_range[1]
            assert length >= self.length_range[0]
        else:
            length = int(np.random.choice(np.array([*self.length_range])))
        log_diffusion = np.random.uniform(-2, 2)
        # print("N = %d" % length)
        trajs_params = {
            "params": params,
            "length": length,
            "log_diffusion": log_diffusion,
            "noise": self.noise,
        }

        return trajs_params

    def get_raw_traj(self, traj_params):

        if traj_params["params"]["tau"] == np.inf:
            raw_pos = np.reshape(
                fbm(
                    n=traj_params["length"] - 1,
                    hurst=traj_params["params"]["alpha"] / 2.0,
                    length=traj_params["length"],
                ),
                (-1, 1),
            )
        else:
            print("Using linalg to generate fBM")
            raw_pos = get_fbm(
                D=1.0,
                alpha=traj_params["params"]["alpha"],
                T=traj_params["length"],
                tau=traj_params["params"]["tau"],
            )
            assert np.sum(np.isnan(raw_pos)) == 0
            # raw_pos = np.empty((traj_params["length"], 1))
        return raw_pos

    def apply_diffusion_and_force(self, raw_pos, traj_params):

        if np.sum(np.isnan(raw_pos)) > 0:
            return raw_pos
        diffusion = np.power(10, traj_params["log_diffusion"])
        raw_pos *= np.sqrt(diffusion * self.time_delta)

        noisy_pos = raw_pos + np.random.randn(*raw_pos.shape) * traj_params[
            "noise"
        ] * np.sqrt(diffusion * self.time_delta)
        return noisy_pos

    def get_traj(self, seed, return_raw=False):

        # print("Get traj %d" % seed)
        np.random.seed(seed)
        traj_params = self.get_traj_params()
        raw_pos = self.get_raw_traj(traj_params)
        pos = self.apply_diffusion_and_force(raw_pos, traj_params)
        if not return_raw:
            return pos, traj_params
        else:
            return pos, traj_params, raw_pos

    def get(self, idx):
        seed = idx + self.seed_offset
        noisy_pos, traj_params, raw_pos = self.get_traj(seed=seed, return_raw=True)

        traj_info = traj_params["params"]
        traj_params.pop("params")
        traj_info.update(traj_params)
        # print("traj_info : %d" % traj_info["seed"])

        assert noisy_pos is not None
        data = TrajData(
            noisy_pos,
            graph_info=self.graph_info,
            traj_info=traj_info,
            original_positions=raw_pos,
        )
        if self.vary_tau:
            data.theta = torch.Tensor(
                [traj_info["log_tau"], traj_info["alpha"]], device=data.x.device
            ).view(1, 2)
        else:
            data.theta = torch.Tensor(
                [traj_params["log_diffusion"], traj_info["alpha"]], device=data.x.device
            ).view(1, 2)

        # print("Returning data %d" % idx)
        return data
