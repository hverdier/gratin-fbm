from .skeleton_data import *


def TrajData(
    raw_positions, graph_info={}, traj_info={}, original_positions=None
) -> SkeletonTrajData:

    assert graph_info["data_type"] == "no_features", BaseException(
        "Unknown data type : %s" % graph_info["data_type"]
    )

    return SkeletonTrajData(
        raw_positions,
        graph_info=graph_info,
        traj_info=traj_info,
        original_positions=original_positions,
    )
