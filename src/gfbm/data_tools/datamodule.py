from torch_geometric.data import DataLoader
import numpy as np
from . import *
import pytorch_lightning as pl
from torch.utils.data import random_split
from torch.utils.data.distributed import DistributedSampler

import matplotlib.pyplot as plt

# from torch_geometric.data import Data
# import torch
import numpy as np

from gfbm.data_tools.diverse import *
from gfbm.data_tools.dataset import fBMTrajDataSet


class DataModule(pl.LightningDataModule):
    def __init__(
        self,
        dl_params={},
        ds_params={},
        graph_info={},
        no_parallel=True,
        validate_on_long=False,
    ):

        super().__init__()

        self.ds_params = dict(default_ds_params)
        self.graph_info = dict(default_graph_info)
        self.ds_params.update(dict(ds_params))
        self.graph_info.update(dict(graph_info))
        self.dl_params = dict(dl_params)

        self.batch_size = dl_params["batch_size"]
        self.epoch_count = 0
        self.validate_on_long = validate_on_long

        self.round = 0
        self.no_parallel = no_parallel

    def setup(self, stage=None, plot=False):
        self.prepare_datasets(stage=stage)

    def prepare_datasets(self, stage=None, plot=False):
        print("Prepare datasets")
        if stage is None:
            print("stage is None, strange...")
        N = self.ds_params["N"]
        dim = self.ds_params["dim"]
        noise_range = self.ds_params["noise_range"]
        model_types = self.ds_params["RW_types"]
        force_range = self.ds_params["force_range"]
        length_range = self.ds_params["length_range"]
        time_delta = self.ds_params["time_delta"]

        args = {
            "N": N,
            "graph_info": self.graph_info,
            "noise": self.ds_params["noise"],
            "vary_tau": self.ds_params["vary_tau"],
            "alpha_range": self.ds_params["alpha_range"],
            "length_sampling": self.ds_params["length_sampling"],
            "length_range": length_range,
        }

        if stage == "fit" or stage is None:
            args["seed_offset"] = N * self.epoch_count
            print("Creating new train fBMTrajDataset, seed=%d" % args["seed_offset"])
            self.ds_train = fBMTrajDataSet(**args)

        elif stage in ["test", "predict"] or stage is None:
            args["seed_offset"] = N * (self.epoch_count + 1)
            ds = fBMTrajDataSet(**args)
            self.ds_test = ds
        elif stage == "validation":
            args_val = {}
            args_val.update(dict(args))
            args_val["N"] = N // 50
            args_val["alpha_range"] = (0.7, 1.0, 1.3)
            args_val["length_sampling"] = "uniform"
            if self.validate_on_long:
                args_val["length_range"] = (
                    int(args["length_range"][1] / 10),
                    int(args["length_range"][1] / 3),
                    args["length_range"][1],
                    3 * args["length_range"][1],
                    10 * args["length_range"][1],
                )
            args_val["seed_offset"] = 0
            self.ds_val = fBMTrajDataSet(**args_val)
        else:
            print("Unknown stage %s" % stage)
        self.round += 1

    def train_dataloader(self):
        # print(f"Call train_dataloader for the {self.epoch_count}th time")
        self.prepare_datasets(stage="fit")
        print("Train dataloader, epoch = %d" % self.epoch_count)
        self.epoch_count += 1
        return DataLoader(
            self.ds_train,
            num_workers=self.dl_params["num_workers"],
            drop_last=True,
            batch_size=self.batch_size,
            sampler=DistributedSampler(self.ds_train) if not self.no_parallel else None,
            pin_memory=True,
        )

    def predict_dataloader(self, no_parallel=False):
        return self.test_dataloader(no_parallel=no_parallel)

    def val_dataloader(self):
        print("Validation dataloader")
        self.prepare_datasets(stage="validation")
        return DataLoader(
            self.ds_val,
            num_workers=self.dl_params["num_workers"],
            drop_last=True,
            batch_size=self.batch_size
            // 4,  # (parce qu'il y a des longueurs plus grandes)
            sampler=DistributedSampler(self.ds_val) if not self.no_parallel else None,
            pin_memory=True,
        )

    def test_dataloader(self, no_parallel=False):
        self.setup(stage="test")
        print("Test dataloader")
        if no_parallel:
            # Otherwise, error Default process group is not initialized
            return DataLoader(
                self.ds_test,
                num_workers=self.dl_params["num_workers"],
                batch_size=self.batch_size,
                drop_last=True,
                pin_memory=True,
            )
        else:
            return DataLoader(
                self.ds_test,
                num_workers=self.dl_params["num_workers"],
                batch_size=self.batch_size,
                drop_last=True,
                sampler=DistributedSampler(self.ds_test)
                if not self.no_parallel
                else None,
                pin_memory=True,
            )
