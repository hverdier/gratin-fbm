# from .diverse import *
from unicodedata import bidirectional
from torch_geometric.utils.degree import degree
from gfbm.layers.diverse import MLP
import numpy as np
import torch
import torch.nn as nn
from torch_geometric.nn import (
    NNConv,
    global_mean_pool,
    GCNConv,
    GINConv,
    GINEConv,
    MessagePassing,
    GlobalAttention,
    InstanceNorm,
)
from torch_sparse import matmul
from torch_geometric.utils import num_nodes, subgraph
from torch.nn import LSTM, Conv1d, BatchNorm1d
from torch.nn.utils.rnn import pad_packed_sequence, pack_sequence, pack_padded_sequence


class MinimalJumpsConv(MessagePassing):
    def __init__(
        self,
        out_channels,
        x_dim,
        dropout=0.0,
        aggr="mean",
        moments=[1],
        f_inner_width=[128, 64],
        **kwargs
    ):
        super(MinimalJumpsConv, self).__init__(
            aggr=aggr, **kwargs
        )  # , flow="target_to_source"
        self.out_channels = out_channels
        self.p = dropout
        self.moments = moments
        M = len(moments) + 1
        self.bn_x = nn.BatchNorm1d(x_dim)

        MLP_size = [M * x_dim] + f_inner_width + [out_channels]
        if M > 1:
            self.f = nn.Sequential(
                nn.BatchNorm1d(M * x_dim), MLP(MLP_size, dropout=dropout)
            )  # last_activation=nn.Tanh()))
        else:
            self.f = MLP(MLP_size, dropout=dropout)

        nparams = 0
        for n, p in self.named_parameters():
            np_ = np.product(np.array([s for s in p.shape]))
            nparams += np_
        print("f size = ", MLP_size)
        print(
            "Convolution has %d parameters. Input dim is %d, output is %d"
            % (nparams, x_dim, out_channels)
        )

    def forward(self, x, edge_index):  # removed edge_attr

        x = self.bn_x(x)

        neighbors_message = self.propagate(x=x, edge_index=edge_index)

        neighbors_message = torch.cat(
            [x] + [torch.pow(neighbors_message, m) for m in self.moments], dim=1
        )
        result = self.f(neighbors_message)

        return result

    def message_and_aggregate(self, adj_t, x):
        # print("message and aggregate")
        return matmul(adj_t, x, reduce=self.aggr)


class TrajsEncoder(nn.Module):
    def __init__(
        self,
        traj_dim: int,
        x_dim: int,
        e_dim: int,
        n_c: int = 64,  # Number of convolution kernels
        n_scales: int = 1,
        latent_dim: int = 8,
    ):
        super(TrajsEncoder, self).__init__()
        e_latent_dim = 16
        x_latent_dim = 16

        self.nodes_bn1 = InstanceNorm(in_channels=x_dim, momentum=0.99)
        self.nodes_bn2 = InstanceNorm(in_channels=n_c, momentum=0.5)
        self.nodes_bn3 = InstanceNorm(in_channels=n_c, momentum=0.5)
        self.nodes_bn4 = InstanceNorm(in_channels=n_c, momentum=0.5)
        self.nodes_bn5 = BatchNorm1d(3 * n_c, momentum=0.5)

        self.edges_bn_1 = InstanceNorm(in_channels=e_dim, momentum=0.99)
        self.edges_bn_2 = InstanceNorm(in_channels=e_latent_dim, momentum=0.5)

        self.nodes_MLP = MLP([x_dim, 128, 128, x_latent_dim])

        self.edges_MLP = MLP([e_dim, 128, 128, e_latent_dim])

        self.att_conv = GINConv(nn=MLP([x_latent_dim, 64, 64, n_c]))

        # self.att_conv = GCNConv(
        #    in_channels=x_latent_dim,
        #    out_channels=n_c,
        #    improved=True,
        #    add_self_loops=False,
        #    flow="target_to_source",
        # )

        # self.att_conv = GENConv(in_channels=x_latent_dim, out_channels=n_c, edge_dim=e_latent_dim)
        # self.att_conv = NNConv(in_channels=x_latent_dim, out_channels=n_c,
        #                       nn=MLP([e_latent_dim, e_latent_dim * 2, n_c * x_latent_dim]), aggr="mean")

        # self.conv_edges = NNConv(
        #    in_channels=n_c,
        #    out_channels=n_c,
        #    nn=MLP([e_latent_dim, e_latent_dim * 2, n_c * n_c]),
        #    aggr="mean",
        # )

        # self.last_conv = NNConv(
        #    in_channels=n_c,
        #    out_channels=n_c,
        #    nn=MLP([e_latent_dim, e_latent_dim * 2, n_c * n_c]),
        #    aggr="mean",
        #    # aggr="max",
        # )

        self.conv_edges = GINEConv(nn=MLP([n_c, 128, 128, n_c]), edge_dim=e_latent_dim)
        self.last_conv = GINEConv(nn=MLP([n_c, 128, 128, n_c]), edge_dim=e_latent_dim)

        # self.last_conv = MinimalJumpsConv(n_c, n_c, aggr="max")

        gate_nn = MLP([3 * n_c, 128, 1])
        # Si le pooling est une simple moyenne, les gradients transmis aux convolutions sur graphes sont très petits.
        # Je n'ai pas encore trouvé d'explication...
        self.pooling = GlobalAttention(gate_nn=gate_nn)
        # self.pooling = global_mean_pool

        self.n_scales = n_scales
        self.traj_dim = traj_dim

        self.mlp = MLP(
            [(3 * n_c) + n_scales, 128, latent_dim],
            use_batch_norm=True,
            use_weight_norm=False,
        )  # used to be tanh for last_activation

    def forward(self, data):
        # adj_t is sparse
        sparse_adj_t = data.adj_t
        row, col, edge_attr = sparse_adj_t.t().coo()
        edge_index = torch.stack([row, col], dim=0)

        x0 = self.nodes_bn1(data.x, batch=data.batch)
        x0 = self.nodes_MLP(x0)

        edge_attr = self.edges_bn_1(edge_attr, batch=data.batch[row])
        edges_embedding = self.edges_MLP(edge_attr)
        edges_embedding = self.edges_bn_2(edges_embedding, batch=data.batch[row])
        sparse_adj_t = sparse_adj_t.set_value(edges_embedding, layout="coo")

        assert data.x.shape[0] == x0.shape[0]
        x1 = self.att_conv(
            x=x0,
            edge_index=edge_index,  # sparse_adj_t,
            # edge_index=sparse_adj_t.t()
        )
        x1 = self.nodes_bn2(x1, batch=data.batch)
        assert torch.sum(torch.isnan(x1)) == 0, print(x0, row, col)
        assert x1.shape[0] == x0.shape[0]

        # We pass the transpose because
        # we want to pass messages from target to source
        try:
            x2 = self.conv_edges(x=x1, edge_index=sparse_adj_t.t())
        except:
            row, col, edge_attr = sparse_adj_t.t().coo()
            d_in = degree(row, num_nodes=x1.shape[0])
            d_out = degree(col, num_nodes=x1.shape[0])
            print(d_in.min(), d_in.max())
            print(d_out.min(), d_out.max())
            print(data.to_keep)
            raise

        x2 = self.nodes_bn3(x2, batch=data.batch)

        assert torch.sum(torch.isnan(x2)) == 0, x1

        # We pass the transpose because
        # we want to pass messages from target to source
        x3 = self.last_conv(x=x2, edge_index=sparse_adj_t.t())
        x3 = self.nodes_bn4(x3, batch=data.batch)

        assert torch.sum(torch.isnan(x3)) == 0, x2

        x = torch.cat([x1, x2, x3], dim=1)
        # x = x1 + x2 + x3

        x = self.pooling(x=x, batch=data.batch)
        x = self.nodes_bn5(x)

        # print(x.size())

        if self.n_scales > 0:
            assert data.scales.shape[1] == self.n_scales
            x = torch.cat((x, torch.log(data.scales + 1e-5)), dim=1)

        out = self.mlp(x)

        return out


class TrajsEncoderLSTM(nn.Module):
    def __init__(self, latent_dim: int = 8):
        super(TrajsEncoderLSTM, self).__init__()
        self.conv1 = Conv1d(1, 64, kernel_size=8, padding="same", padding_mode="zeros")
        self.conv2 = Conv1d(64, 64, kernel_size=8, padding="same", padding_mode="zeros")
        self.lstm = LSTM(
            bidirectional=True, num_layers=4, input_size=64, hidden_size=32
        )
        self.dense = MLP([32 * 2 * 4, latent_dim])

    def forward(self, x):

        # pos_vecs = [x.pos[x.batch == b] for b in np.arange(x.theta.shape[0])]
        pos_vecs = pack_sequence(
            [x.pos[x.batch == b] for b in np.arange(x.theta.shape[0])],
            enforce_sorted=False,
        )
        pos_vecs_padded, length = pad_packed_sequence(pos_vecs, batch_first=True)
        pos_vecs_padded = torch.swapdims(pos_vecs_padded, 1, 2)

        # print(pos_vecs_padded.shape) # torch.Size([32, 1, 909])
        batch_size = pos_vecs_padded.shape[0]
        h = self.conv1(pos_vecs_padded)
        # print(h.shape) # torch.Size([32, 64, 909])
        h = self.conv2(h)
        # print(h.shape) # torch.Size([32, 64, 909])
        h = torch.swapdims(h, 1, 2)
        # print(h.shape) # torch.Size([32, 909, 64])
        h = pack_padded_sequence(
            h, lengths=length, batch_first=True, enforce_sorted=False
        )
        _, (h, c) = self.lstm(h)
        h_forward = h[:batch_size]
        h_backward = h[batch_size:]
        h = torch.cat((h_forward, h_backward), dim=0)
        h = torch.swapaxes(h, 0, 1)
        # print(h.shape) # torch.Size([32, 8, 32])
        h = h.reshape(batch_size, -1)
        # print(h.shape) # torch.Size([32, 256])
        h = self.dense(h)
        return h
