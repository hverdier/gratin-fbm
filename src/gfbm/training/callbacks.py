from pytorch_lightning.callbacks import Callback
import pytorch_lightning as pl
import numpy as np
import matplotlib.pyplot as plt
import torch


class fBMPlotter(Callback):
    def on_init_end(self, trainer):
        self.tb = trainer.logger.experiment
        self.round = 0

    def on_validation_epoch_start(self, trainer, pl_module):
        self.true_alpha = []
        self.pred_alpha = []
        self.length = []

    def on_validation_batch_end(
        self, trainer, pl_module, outputs, batch, batch_idx, dataloader_idx
    ):
        preds, true, length = outputs
        self.pred_alpha.append(torch.mean(preds[:, 1], dim=-1).detach().cpu())
        self.true_alpha.append(true[:, 1].detach().cpu())
        self.length.append(length[:, 0].detach().cpu())

    def on_validation_epoch_end(self, trainer, pl_module: pl.LightningModule) -> None:

        true_alpha = torch.cat(self.true_alpha, dim=0)
        pred_alpha = torch.cat(self.pred_alpha, dim=0)
        length = torch.cat(self.length, dim=0)
        se = (true_alpha - pred_alpha) ** 2

        fig = plt.figure()

        unique_length = torch.unique(length)
        plt.plot(unique_length, [torch.mean(se[length == l]) for l in unique_length])
        plt.yscale("log")
        plt.xscale("log")

        self.tb.add_figure("SE_length", fig, pl_module.global_step)
